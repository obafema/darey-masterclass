1. About Helm 
2. Accessing services in kubernetes through the Ingress
   1. Deploying the Ingress Controller with Helm
   2. Connecting DNS(Route53) with Ingress Controller Load Balancer (Proxy)
3. Deploy an application into the cluster (Jenkins) 
   1. Helm to install Jenkins
   2. Ingress resource for jenkins
4. Secure the application with TLS
   1. Deploy Cert-manager
   2. Deploy Certificate Cluster Issuer
   3. Configure the ingress to automatically secure the service


Preparing an infrastructure for CI/CD
--------------
1. Talk about CI/CD 
2. Pre-requisite setup
   1. AWS/Terraform setup
   2. Kubernetes cluster
3. Tools required for CI/CD
   1. Jenkins
   2. Artifactory
   3. Cert-Manager
4. Deploy and configure tools
   1. Artifactory
   2. Cert-manager
   3. Jenkins
5. Introduce Kustomize
6. Implement CI/CD using Kustomize to implement into various environments.




Upgrading Artifactory 

  postgresql-password: dE1JTVRYR2x2aQ==      tMIMTXGlvi
  postgresql-postgres-password: aVFvSEFxWXRVbQ==         iQoHAqYtUm

helm upgrade -i my-artifactory \
jfrog/artifactory --set postgresql.postgresqlPassword=${POSTGRES_PASSWORD} \
--set databaseUpgradeReady=true \
-f /Users/dare/Downloads/aws-project-darey/darey-masterclass/week9/live-class/artifactory/values.yaml \
-n tooling


POSTGRES_PASSWORD=pSU8jn5bKi60MpVu4J9odF


helm upgrade -i artifactory jfrog/artifactory --set postgresql.postgresqlPassword=${POSTGRES_PASSWORD}  -f /Users/dare/Downloads/aws-project-darey/darey-masterclass/week9/live-class/artifactory/values.yaml -n tooling --version 7.41.6


helm upgrade -i my-artifactory jfrog/artifactory --set postgresql.postgresqlPassword=${POSTGRES_PASSWORD} --set databaseUpgradeReady=true -f /Users/dare/Downloads/aws-project-darey/darey-masterclass/week9/live-class/artifactory/values.yaml -n tooling

Recap so far 
Deployed Services
1. Cert Manager 
2. Ingress Controller
3. Artifactory
4. Jenkins 
5. How Helm works

TODO

6. Configure RDS for tooling DB
7. Introduce Kustomize for deployments instead of Helm
8. Base and Overlays
9. Introduce External DNS and why it is better than configuring DNS from Terraform
10. Add External DNS annotations to Ingress
11. Connect Tooling app with the database - https://github.com/darey-devops/tooling/blob/master/Dockerfile#L4
12. Update K8s deployment to overide the container defaults 
13. Test that the app works in all environment
14. Evaluate secret management issues and Introduce Vault
    1.  Installing Vault
    2.  Connecting Vault with Kubernetes
    3.  An overview of Vault KV secret engine
    4.  Dynamic Vault Secret injection through a side car container
    5.  Annotations used to invoke dynamic secret injection into containers
    6.  Check the injected secret in the /vault/secrets path
    7.  Check what the application sees when the exported secret is used by the app process "cat /proc/1/environ | tr '\0' '\n'"
15. Quick overview of what the ideal pipeline will look like when developers work, and features are released into production

helm upgrade -i mysql bitnami/mysql --version 9.4.4 -n masterclass-practice
MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace masterclass-practice mysql -o jsonpath="{.data.mysql-root-password}" | base64 -d)

kubectl run mysql-client --rm --tty -i --restart='Never' --image  docker.io/bitnami/mysql:8.0.31-debian-11-r10 --namespace masterclass-practice --env MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD --command -- bash


mysql -h mysql.masterclass-practice.svc.cluster.local -uroot -p"sA0cDrKyPK"

export sql_file=/Users/dare/Downloads/aws-project-darey/k8s-part3/tooling-app/tooling/html/tooling_db_schema.sql

kubectl -n masterclass-practice exec -i mysql-0 -- mysql -u root -p"$MYSQL_ROOT_PASSWORD" < $sql_file








1.  Update kustomize to use Helm
2.  Complete Jenkins pipeline for tooling
   1. docker build and push
   2. kustomize deploy to environments

Next steps 

1. GitOps
   1. YAML Pipelines - GitlabCI or Github actions
   2. ArgoCD
   3. Vault Integration and Dynamic Secrets Injection
   4. Quality Gate in CI Pipelines


cat /proc/1/environ | tr '\0' '\n'




