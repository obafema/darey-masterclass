  -- helm repo add hashicorp https://helm.releases.hashicorp.com
  -- helm repo update 
  -- helm search repo hashicorp/vault -l
  -- Configure auto unseal https://learn.hashicorp.com/tutorials/vault/autounseal-aws-kms
       -- Create KMS key 

  -- helm upgrade -i vault hashicorp/vault -f /Users/dare/Desktop/gitlab/darey.io/darey.io-PBL/2022/pbl-expert/projects/project25/vault/values.yaml

# Check if Vault is sealed - Must be false
kubectl exec vault-0 -- vault status -format=json | jq -r '.sealed'

# Check if vault is initialized - Must be true
kubectl exec vault-0 -- vault status -format=json | jq -r '.initialized'

  # Vault Init
kubectl exec -ti vault-0 -- vault operator init -format=json -n 1 -t 1

  "recovery_keys_b64": [
    "12zMf5Kd7qVxykysBO1swA4AGgEcMAAGAlI8qNA0aL3D",
    "MIvSBmxO4+CL6KG9+nMmXhxO7mTzkw7KJ6pkIAqm29ia",
    "x3U8uZj4bMTCPwTML6yCK8Dgu3DgWTi2GleXzUpmvsXK",
    "61GBexKU1YipnwXt6DNpYbWYS810ZHqeUDWRnlcZBHDI",
    "J/lP/fAhdNsszlyH0LYhiIbgU7Zxet3HNGhc7kqOWW+k"
  ],
  "recovery_keys_hex": [
    "d76ccc7f929deea571ca4cac04ed6cc00e001a011c30000602523ca8d03468bdc3",
    "308bd2066c4ee3e08be8a1bdfa73265e1c4eee64f3930eca27aa64200aa6dbd89a",
    "c7753cb998f86cc4c23f04cc2fac822bc0e0bb70e05938b61a5797cd4a66bec5ca",
    "eb51817b1294d588a99f05ede8336961b5984bcd74647a9e5035919e57190470c8",
    "27f94ffdf02174db2cce5c87d0b6218886e053b6717addc734685cee4a8e596fa4"
  ],
  "recovery_keys_shares": 5,
  "recovery_keys_threshold": 3,
  "root_token": "s.zFOSK8xWAEAIU2pvKx3oj4hc"


Review this for auto unseal - /Users/dare/Desktop/gitlab/darey.io/darey.io-PBL/pbl-expert/projects/project23/wip/vault/values.yaml

vault operator init

Recovery Key 1: +kescUriLYSmBL094cKwfqFnsLPBTk87h+CHV1UfoN/W
Recovery Key 2: UZcpNXYfWNCkRR4N36K2yMlaYxcpJDhCtcWzZiqLqP3e
Recovery Key 3: Piufpu62ONwmlK3tNG5K/79XABZXjo+GFiWJfQkT577f
Recovery Key 4: b4zulMj0qnE+q4dV8e7OiS+RgmUZD9ZwObGFWI1OBxsa
Recovery Key 5: RsOo82wrPB+5aVAQzr5+LDHs+npH2i80P+IDUJVaR0Fe

Initial Root Token: s.4RDITxT3ioJD8QEwNUZ0i8mB

vault operator unseal +kescUriLYSmBL094cKwfqFnsLPBTk87h+CHV1UfoN/W
vault operator unseal RsOo82wrPB+5aVAQzr5+LDHs+npH2i80P+IDUJVaR0Fe
vault operator unseal Piufpu62ONwmlK3tNG5K/79XABZXjo+GFiWJfQkT577f




  Notes

  -- default installation is not scalable as it uses file backend. For HA use consul 



  ------- Connect Vault to Kubernetes ----------
Parent documentation 
https://www.hashicorp.com/products/vault/kubernetes

-- Configure Ingress and DNS for Vault UI 

1. Create vault service account 
   kubectl apply -f /Users/dare/Desktop/gitlab/darey.io/darey.io-PBL/2022/pbl-expert/projects/project25/vault/yamls/vault-auth-service-account.yaml

2. Create vault cluster role binding to bind with the  system:auth-delegator ClusterRole
   NOTE: you must provide the namespace subjects section
kubectl apply -f /Users/dare/Desktop/gitlab/darey.io/darey.io-PBL/2022/pbl-expert/projects/project25/vault/yamls/vault-auth-cluster-role-bind.yaml


3. Enable secret kubernetes auth method in Vault
    kubectl exec -ti vault-0 -- sh
    export VAULT_TOKEN=s.zFOSK8xWAEAIU2pvKx3oj4hc
    kubectl exec -ti vault-0 -- vault auth enable kubernetes

4. Enable secret engine

vault secrets enable -version=2 kv

5. Add secret to vault 

vault kv put kv/tooling/dev MYSQL_PASS=od2r847qGM

6. Create a policy in Vault to allow access to the secret created above

    export VAULT_ADDR=https://vault.sandbox.svc.darey.io 
    export VAULT_TOKEN=s.zFOSK8xWAEAIU2pvKx3oj4hc

   $ vault policy write tooling-app /Users/dare/Desktop/gitlab/darey.io/darey.io-PBL/2022/pbl-expert/projects/project25/vault/yamls/vault-policy.hcl

    vault policy write tooling-app2 /Users/dare/Desktop/gitlab/darey.io/darey.io-PBL/2022/pbl-expert/projects/project25/vault/yamls/vault-policy2.hcl

7. Configure access to the kubernetes auth method created earlier

     Set environment variables

        -- Start the kube api server  through a proxy mapped to localhost 

        kubectl proxy --port=8087

        OIDC_ISSUER=$(curl --silent http://127.0.0.1:8087/api/v1/namespaces/default/serviceaccounts/default/token \
        -H "Content-Type: application/json" \
        -X POST \
        -d '{"apiVersion": "authentication.k8s.io/v1", "kind": "TokenRequest"}' \
        | jq -r '.status.token' \
        | cut -d. -f2 \
        | base64 -d | jq .iss)

    # Set VAULT_SA_NAME to the service account created earlier
    $ export VAULT_SA_NAME=$(kubectl get sa vault-auth -o jsonpath="{.secrets[*]['name']}")

    # Set SA_JWT_TOKEN value to the service account JWT used to access the TokenReview API
    $ export SA_JWT_TOKEN=$(kubectl get secret $VAULT_SA_NAME -o jsonpath="{.data.token}" | base64 --decode; echo)

    # Set SA_CA_CRT to the PEM encoded CA cert used to talk to Kubernetes API
    $ export SA_CA_CRT=$(kubectl get secret $VAULT_SA_NAME -o jsonpath="{.data['ca\.crt']}" | base64 --decode; echo)

    # Look in your cloud provider console for this value
    $ export K8S_HOST=https://D240F067A0BBB614E86F05D55A59E65B.gr7.eu-west-2.eks.amazonaws.com


vault write auth/kubernetes/config \
        token_reviewer_jwt="$SA_JWT_TOKEN" \
        kubernetes_host="$K8S_HOST" \
        kubernetes_ca_cert="$SA_CA_CRT"
        issuer=$OIDC_ISSUER


# Create a role named, 'tooling-app' to map Kubernetes Service Account to
# Vault policies and default token TTL
$ vault write auth/kubernetes/role/tooling-app \
        bound_service_account_names=vault-auth \
        bound_service_account_namespaces='dare' \
        policies=tooling-app \
        ttl=1440h


1. Test that the vault-agent-injector is working fine 

```
 kubectl get po
NAME                                   READY   STATUS    RESTARTS   AGE
vault-0                                1/1     Running   0          4h50m
vault-agent-injector-975ffdf5d-nnbkh   1/1     Running   0          2m42s
```

Troubleshooting tips

-- ensure the vault-agent-injector service account has necessary permissions 
```
kubectl apply -f /Users/dare/Desktop/gitlab/darey.io/darey.io-PBL/2022/pbl-expert/projects/project25/vault/configure-vault-k8s-auth/clusterrole.yaml
```

9. Apply a test deployment 

```
kubectl apply -f /Users/dare/Desktop/gitlab/darey.io/darey.io-PBL/2022/pbl-expert/projects/project25/vault/deployment.yaml
```

Possible errors - Permission denied error accessing  the path in Vault 

```
kubectl logs  web-deployment-57675fc8f6-nnq55 -c vault-agent-init 
```

```
URL: GET http://vault.dare.svc:8200/v1/secret/data/tooling
Code: 403. Errors:

* 1 error occurred:
        * permission denied

 (retry attempt 7 after "16s")
2022-05-11T21:05:47.154Z [WARN] (view) vault.read(secret/data/tooling): vault.read(secret/data/tooling): Error making API request.
```



Example dynamic vault injector 
https://www.vaultproject.io/docs/platform/k8s/injector/examples


Role: Email marketing manager

Job description:

-Manage and build overall email marketing strategy
- Recommend marketing strategy for crafting tailored offer to target audience 
-Create, test various email lists and campaigns success
- Create automation sequences
-Determine email marketing KPIs and segment list for prompt actions
-Develop a lead generation strategy.
-Identify the target audience and grow email list
-Analyse data to get smart insights and Suggest methods for improvements
-Develop documentation and road maps for processes, A/B tests, and promotions that succeed through email.
- Maintain email hygiene 




0FTU4CA6D8FC666P
/Users/dare/.gnupg/trustdb.gpg
/Users/dare/.gnupg/openpgp-revocs.d/56561175BFC9ECADBDBE1DAB31D7E15559FA7FE0.rev
31D7E15559FA7FE0 marked as ultimately trusted

/Users/dare/.gnupg/openpgp-revocs.d

pub   rsa3072 2022-08-05 [SC] [expires: 2024-08-04]
      56561175BFC9ECADBDBE1DAB31D7E15559FA7FE0
uid                      darey.io <dare@darey.io>
sub   rsa3072 2022-08-05 [E] [expires: 2024-08-04]


-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: Keybase OpenPGP v2.1.13
Comment: https://keybase.io/crypto

xsFNBGLsycsBEADVlUpMSo/0PrJV5qo22+4Y8rwwUD1WbFbr+9JUclrQM6+Eeg4R
WwfjGpXwHfVYqnaH1VsvwXSHtEXpiasKyU8h6Zhzzx41NbOQVAMkTi5Q8CQTLEZe
fbkTODpwFU/we4XKc0MRHRMaqcn/LBWFp2DfIbHfhv0WlEReJCvNvz1gPCA4F+dR
qGA5djRSO/J2leZMEy3FiH5bSFWvxTmmngxalCy/mAjE+TB7Gys1+gmEoB6CYMNi
qn+HLt1q6JQ//+XcgegpMvtPONtCEHzOx6hVxXdPyQU2Y0apifP+ReuUt9k/TmZy
iB6h7zicCQ0O6V3Tm5ihzElWv9bqGmZZEwwm3Lv3o2taE+Ew3dtSCUCPZENi+Hmw
Kpm1PODxiTrJn2yDzQUmxhVBoOM+lDFyiAcVCaGJUpnDlbHUYlWwiZKB9G4j/sT6
h6n5CoCvp+QTPqiZ4fNPgOpeYD/i3UwF6s7TsBehmx4gyHBJ01PWxytSdVIsvO0H
l18dV2gn7JTPlXbySnl057KPH/HkcpW506g3+uXs9pCfVScQnLUN4O7pmgWZ24cM
N01k1boGyxQpLoRYTYJTFqxLNzzqTLf27X4P2kTLXQu4EIsyr8CRbYzI5ui3Wrlq
XKCueyU+xariMsOY/IC5/gy+5vcNdmbukgwKAYA/8MsY/yxJYItPf/OpqwARAQAB
zSFEYXJlIE9sdWZ1bm1pbGF5byA8ZGFyZUBkYXJleS5pbz7CwXQEEwEKAB4FAmLs
ycsCGwMDCwkHAxUKCAIeAQIXgAMWAgECGQEACgkQ/WvzqbgTDCjLMhAAlyf30XHL
i4/2yJfEgmjFIc7KPfcyYLARe0OdA/+esMvk5dc/dm2SqeCowRh6e9waD6UetkKO
GZ2jZjFGc0apQ0bLMbY/sfPMABI9wpc4J9SuENwau+giQCudpxj88lzLEMCLH0hJ
9tNwpf+tdsp1R241E/IKZLd877Tw0JmhquVTqCxBJu4D8pteSqyHNoXdrrEP+qeh
MTyjHwmXXrPpLCp6UDwn/DLRlo3+erXpuAKuJGJ2posoM9yq19kv9cKS2V5RakBF
zO2rjOZmpP1B63dy135Q6YGzzmdPnQZFLc6paTTe7rVw8za/UdPoO/LC/aCdPytH
dOHlyYapwRGvSTDWFElSESuZc114vRxBe22SSepw+328FLHi6cH4wYfPquSkK0xd
ocZ+xUeIf6xTc50H5siMbPZWlfKqn5IDdYI/mUJmJ6AwnES3sN5od+ENfSj9JUTQ
/6181kmBF7oX1MrY2M3FJym934H1GDORQXSIkLL9zsKLP106XKfnvRgWTeCX7RF0
1JwtcdoO7I3I67VG2/uJM/o8VUAJpUAMZkBvOmHQDycBQIkfnGcJkYLui9nPta/l
S/CYEyu+1TiNEa52tPlJelaBKDKvG4RO0jcXwb+sjGeO59gfcEEaxQgErJcg208+
jBPG+b/Q4BMVwoSYEy5dJOPhYan7CZVikNHNJkRhcmUgT2x1ZnVubWlsYXlvIDxk
cmUwMDJuZ0BnbWFpbC5jb20+wsFxBBMBCgAbBQJi7MnLAhsDAwsJBwMVCggCHgEC
F4ADFgIBAAoJEP1r86m4EwwonsEP/1d7ZxBXUaOR0MUMdmqYuZZeOdgBou6nb3YC
Z9EPvzU//gf26XqmmY7ahvHomU/4e0ZR7tBoOxBZkw7lAdmM/XfhvbERqeHnMOm2
XyX8lwYp0p19Q67yXAW9a2HH7PwmMrSrHXO7pefXIx17Kb0RAYWDTE/0NnVwD+qa
hltJhpDj6eN3y7r+fUzbIpRgMmKuqJB72J77G6KI5HIaDeNvVyog8nG1FCki6SQY
fF1/lONyKdhu1RhnqSFU9LNVojdiJAl+vFCsxvxOEG61V+omsp0Khp/mGNAIghIp
GxY2j3Xw0iBiC/zlYdwG0DPbVS9lu0TtZfidAlu6Z5f9j2X2NiM9NbgXh9eW7wKT
2WzUMz9L1M2hKG8OXAYVmONHGbmp+QjeXxhFBy5cDXxNcnPQEOkC9V9xGp9XE8bK
eBiiVHET/dT2NbdmXg+Z0pAFY8HO2X7fiCcKbzXt8zTwgexqcQLDnv5+4SIA2gtv
K/4sABSSckxMBiTzlCYSwl0dgoJjpc9wnOiFpFQDP8AzM5Y1wed09S4hTO2t6SGa
OEyRQFvCUlGgv45lGHxd0/SyGVaLM7GxQ2OeUG+A72P+PhsPQau2Xm1Xy8z3yl82
+7hBBcd9FErYSmCR/z4JYWBHtXvd4JEPfuzzzhf22gQqEwHR1GmDhyl28w/BU1qO
QFb513wQzsBNBGLsycsBCAC4Y9tVw/N6KAG/Cwl8gU6n2LC1+YQjVOwF6puRFJEm
yCbgUx1SoIqG2PjXvS0oh4OWaVPRHqcULvl1y9/YGoqYvSdZ+U5N4iFdoitWL/am
jNYyMsbudxhs/S0agwUaUB/QpC2+AGTNFpDPho/OnnpXIYAwqcAsNwjzSya9llKj
uWQlUt8HnpZOMBVCM5DTbNQLzJQ7DafCXgkCiYaeWTQdv7bdKk5gs7ig8rXLqKHs
N+Gg2JhuritQuqspIfDmNUyD4MtKnDAwvVmxofaakl91g7+Emv9Auj2N0nsMcmVF
GbKw5OdjsX6PCLfwVGyXJjRZPS0HVXnidH1HlX2W5NuVABEBAAHCwoQEGAEKAA8F
AmLsycsFCQ8JnAACGwwBKQkQ/WvzqbgTDCjAXSAEGQEKAAYFAmLsycsACgkQtNLs
ILXt+2gwlwf+L22gTuR6ecKKgQt79y3s0TIEZuBSEF9R9O5vMsxcQiraXmMKadTT
MShTz1JYcJuzVik18Tg4nOYg1a+qDepxKis+GkP2GehH3baPsehKfjdcgv6jCW9L
TIJv8VsQ6KCB4mu3lnR3uR7pz0Fc4j/Gc/UkpmBbnfKbFPDlUVNUATb9Gfqh0qV0
CxQYSY+H138QI4QSZtF7/QCE+hxscejaUeHiEoDDNUQC3ApdtNYcgDXLIR+/TX38
mQp6kDpeDeSjSd0GeDo58caZw+VUNA9rMzozNREQAvL/dnjpEiycRvxZTCiNmwGK
RYrBZi5v3eLNBfu7tJYxbrzbJvcW3dmracF6EAC/mKk5UMYbg8DEBOALLLrMLnem
+D6HQ3HgrZfkf1+Ct0MAa1iOz8tRKn+frU7hI5vlyYrGiwrwj8bwssxOCGRDrMtQ
bLANgN7sTr5wG6BZRJiSyrAQBdUDumHbXPHLufEkLVNEsDDD6nYD4fxo5VCyhEmB
6YZBu3XYppq3HaFlG/BLL+SyZWrAdPkpVzVOcDk3h5g765BBxhGLlPnVIkDF6me6
pRat3X+Rs7RYRN0gfM51qc5jyb7RXzmbHJtXFEFmqO7VCokrKT5JZuEkdfDpwIV5
djIgU70yj5wqRrBmDTc8LdnzAIFQr8iwg5YN6sgTDZE3iUS71UkuZN9nKEJR8FD3
tvkgUEhNY3nMhRZaqeAb6Hodzo+Bbumv5YOGEr6bxco9vsKldOeESe8aMDLP/VSM
73s5yp4a8z/9Y3gny2LqGq9TwezLAB+GOtHEfuctJuKXLhWqvPBhJNr2dHodr8TI
ievVqT7V6TqWGnVYIdk73CcMcUKdsPE0a1LtZ37f7hg/dJtjkwqlTJ7l+yrbRmMK
WjHpRm9BtbE/mIPghNm+TeYklmDQ7G1co4Gs8sn4YTc9sHg+Lr1AMRn9472XTHdj
qVMLruVjvY1Her3NfsyV55inxalQkshFHp84gaMi4rsHGG8/4pdbidiVyt2gTMcG
zRIyn41XL7yJvLmnCM7ATQRi7MnLAQgA0DSGXaODVp5E5quUMAQvvyBapp8CtVSq
iqQWXyZBkle6+pjYJTtpoxDSN3JqSPCGkF9XnzwVr1PRsPqrZ5etgv3Z6hrmvTaI
9hozRsLY6k70aI/w3cddJ+Sgw9GBMmjOdheQIFwu2puqPwwdR9bJz7RSw7dicqeZ
1UgkBFaLm8YhhfxXpWo7EZpox6+mfhkJIDdvdwSh2nxnYKAu2GTj/4VeOG0KRvY0
hG0Kr6XOkVeQvywjnnPAOzfTt1Jh4ZvmPudtWcAG7WXtc9hTpMmwezV3iFf+f5pw
j9pM0uavt0LEjTskfaz38lsemtF9gXXt/KG1KoOKWTmL4N0nfTmxVwARAQABwsKE
BBgBCgAPBQJi7MnLBQkPCZwAAhsiASkJEP1r86m4EwwowF0gBBkBCgAGBQJi7MnL
AAoJEEB2cTx+78qvR5sH/0fns1tiGBEIEHWvpag3nli5OE2y5NrkZZV7AxuploKg
l06x7YBcsVo77dX5OtZhfSMgaOE4m6p+7s9BC3yBWG80bBW4U/0RNAjmYwahQAQL
8YPLkGDqIyzuHEgu9iPEHCWix5MjEjByPFr5RdJdrEltwBVj1Yrnvu+Y0jEBbPgq
PhE7s8a7dSYYAB4txggGgsmNPBIVCKyPStsa4Yf0fUsNbWFnlHWFuRknc/BtMB1y
WiM59WS+9siGbZZpLsh/fK434QEm6FpLkKgIomza/bX2v3Yo0R756xRDESRx1fAf
ux8LRfkfGX6MeZqYqArY5rTq2IiZT8rZv12YQbUYEvnCdxAAhF+AIRl6TgAT4cWo
yAUpcGlDi8myLXdbCPRAjr1BJhXR4JtDKc6Q75v+SzszlTxlQM8w17Vk4cqFnmtA
yObYChKChU5bcNaebRYXEEJ+6/D/VJ4GcZdiFuHC7r1P0tjBL+BBlXp5sQqaSp2i
RCzCu5ZbNfDfqP/RmDE9yol245DEzvXOUMplvmZ47yDwRwIdbBeSIQ3YxPZrzuH3
S6hoXtJ6dIhpQ9hx4QwxmUydc+aq7nRM0LYFiGl+axKeizATt8MRiUnWb3LgyZAl
+ckHtyg/ozbhPjcYiPABSIQcnufqP8XdsaGntBq6n53G5wgWenEw3vAlKFRLMH+5
BiueRczYdkDGQFQb437qY4d9DXWt17N0Th2IvXwmee1Xh59sy5qx2o/JoAOAPlsh
yDxz0mLmhSvucPqcsLUytv4AZFkvPE7m2SpxCKn/5c1I/dtMg5M3tBfBiojxgXg1
nZxheYOiv5UT/D5Vjlja7DMktJcqLx4LaQUHGdnl6kPguua1NWVaV1LXdHw6RNh9
3zGs7FOuUqp0yXsWxx27JZYgXStAtRNonXoqXO9iXhhbAXPUl5Gpx/Ovw0rHMF+D
FdF3nOlGmsExf8DPZ1B8fTBsebLLDpnmrstqIO2YXjpVWtdLfXBBGCyAGcu1Knai
DP5UZYXft4SM/O4xFyY9tM34W64=
=QYLu
-----END PGP PUBLIC KEY BLOCK-----
